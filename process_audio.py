import pandas as pd
from flask import Flask, render_template
from flask_socketio import SocketIO, emit

import numpy as np
import librosa
import soundfile as sf

import datetime

#from flask_uwsgi_websocket import WebSocket

app = Flask(__name__)
app.config[ 'SECRET_KEY' ]= 'jsbcfsbfjefebw237u3gdbdc'
socketio = SocketIO(app)

@app.route( '/' )
def index():
  return render_template('./index.html')


@socketio.on('send_audio')
def process_audio(json):
      msg = np.array(json['msg'])
      sampleRate = json['sampleRate']
      audio=np.frombuffer(msg, 'i2')
      x = datetime.datetime.now()
      #sf.write('./record/'+  x.isoformat() +'.wav', audio, sampleRate, subtype='PCM_24')
      sf.write('./record/{}.wav'.format(x.isoformat()), audio, sampleRate, subtype='PCM_24')
      mfccs= calmfccs(audio,sampleRate)



def calmfccs(audio,sampleRate):
      try:
            X = librosa.resample(audio, sampleRate, 22050)
            mfccs = np.mean(librosa.feature.mfcc(y=X, sr=sampleRate, n_mfcc=40).T, axis=0)
      except Exception as e:
            print("Error en el audio: {}".format(e))
            return None
      return mfccs




if __name__ == '__main__':
      socketio.run( app, debug = True )