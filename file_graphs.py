import os
import librosa
import librosa.display
import glob
import soundfile as sf
import matplotlib.pyplot as plt

for title in os.listdir("./data"):
    data, samplerate = sf.read('./data/{}'.format(title), dtype='float32')
    data = data.T
    data_22k = librosa.resample(data, samplerate, 22050)
    plt.figure(figsize=(12, 4))
    librosa.display.waveplot(data_22k, sr=samplerate)
    name = title.split(".")[0]
    plt.title(name)
    plt.savefig("./graph/{}.png".format(name))
    plt.close()