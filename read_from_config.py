from keras.models import model_from_json
import soundfile as sf
import librosa
import numpy as np
import pandas as pd
import os
import csv, operator
from sklearn.preprocessing import MinMaxScaler
#from sklearn import metrics
#from sklearn.metrics import (classification_report, cohen_kappa_score,
#                             confusion_matrix, f1_score, precision_score,
#                             recall_score, accuracy_score)
#ABRIR CSV PARA PARA ESCRIBIR EN EL
csvsalida = open('salidatAUDIOS.csv', 'w', newline='')
salida = csv.writer(csvsalida,)
salida.writerow(['Categoria', 'Audio','prediccion'])
d = []
scaler=MinMaxScaler()
directorio="model"
json_file = open(directorio+'/model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights(directorio+"/model.h5")
print("Loaded model from disk")
dategorias = os.listdir("./entrenamiento/")
for categoria in dategorias:
    audios = os.listdir("./entrenamiento/{}".format(categoria))
    d = []
    for audio in audios:
        data, samplerate = sf.read(
            './entrenamiento/{}/{}'.format(categoria,audio), dtype='float32')
        data = data.T
        X = librosa.resample(data, samplerate, 22050)
        mfccs = np.mean(librosa.feature.mfcc(
            y=X, sr=samplerate, n_mfcc=40).T, axis=0)
        #d.append(mfccs)
        #scalerD=scaler.fit_transform(np.array(d))
        #data = pd.DataFrame({'feature': [scalerD[0]]})
        data = pd.DataFrame({'feature': [mfccs]})
        X = np.array(data.feature.tolist())
        #print(X)
        y_pred = loaded_model.predict(X)
        pred = y_pred.argmax(axis=1)
        #print(str(porcentage[int(pred)])+' '+categoria)
        print(str(int(pred))+' '+categoria+' '+audio)
        d.append([categoria,audio,int(pred)])
    salida.writerows(d)
#print("sample end")

del salida
csvsalida.close()