import os
import pandas as pd

data = pd.read_csv("./data.csv", sep=";")

print(data.shape)
print(data.index)
print(data.columns)
print(data.info())

print(data.p.value_counts())