import csv
import operator

import librosa
import numpy as np
import pandas as pd
import soundfile as sf
from keras.layers import (Activation, Convolution2D, Dense, Dropout, Flatten,
                          MaxPooling2D)
from keras.models import Sequential
from keras.optimizers import Adam,sgd
from keras.utils import np_utils, to_categorical
from sklearn import metrics
from sklearn.metrics import (classification_report, cohen_kappa_score,
                             confusion_matrix, f1_score, precision_score,
                             recall_score, accuracy_score)
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from title_procesing import file_names_to_csv2
from random import randint

Data,Category = file_names_to_csv2()

X_train, X_test, y_train, y_test = train_test_split(Data, Category, test_size=0.3,train_size=0.7,shuffle=True, random_state=42)

filter_size = 2

index = 0
score = [100, 0]
while True:
    model_aux = Sequential()

    model_aux.add(Dense(16, input_dim=40, activation='relu'))
    model_aux.add(Dense(256, activation="relu"))
    model_aux.add(Dense(1))
    model_aux.add(Dense(11, activation="softmax"))
#classifier.add(Dense(output_dim = 8 ,init ='uniform',activation = 'softmax'))
    model_aux.compile(loss='categorical_crossentropy',
                metrics=['accuracy'],
                #optimizer='sgd'
                optimizer='adam',
                )
    #model_aux.fit(X_train, y_train, batch_size=5, epochs=100,22
    #          validation_data=(X_test, y_test))


    model_aux.fit(X_train, y_train, batch_size=2, epochs=100,shuffle=True,validation_data=(X_test, y_test))


    score_aux = model_aux.evaluate(X_test, y_test, batch_size=2,verbose=1)
    print('--------------evaluate')
    prin t(score_aux)
    if score_aux[0] < score[0]:
        score = score_aux
        model = model_aux
    if index == 12:
        break 
    index +=1


print("-----model.summary-----")
print(model.summary())
print("------------------")

y_pred = model.predict(X_test)
print("-----accuracy_score-----")
print(accuracy_score(y_test.argmax(axis=1), y_pred.argmax(axis=1)))
print("-----confusion_matrix-----")
print(confusion_matrix(y_test.argmax(axis=1), y_pred.argmax(axis=1)))

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")
