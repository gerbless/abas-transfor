import os
import pandas as pd
import librosa
import numpy as np
import soundfile as sf
from sklearn.preprocessing import MinMaxScaler
from pydub import AudioSegment

def file_names_to_csv():
    categories_dict = {
        '0_':  [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '10_': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '20_': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '30_': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '40_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '50_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '60_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '70_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '80_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '90_': [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        '100_':[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]
    }
    data = []
    categories = []
    taras = os.listdir("./15 kg/")
    for tara in taras:
        porcentages = os.listdir("./15 kg/{}".format(tara))
        for porcentage in porcentages:
            audios = os.listdir("./15 kg/{}/{}".format(tara, porcentage))
            for audio in audios:
                audio_data, samplerate = sf.read(
                    "./15 kg/{}/{}/{}".format(tara, porcentage, audio), dtype='float32')
                data_22k = librosa.resample(audio_data.T, samplerate, 22050)
                mfccs = np.mean(librosa.feature.mfcc(
                    y=data_22k, sr=samplerate, n_mfcc=40).T, axis=0)
                data.append(mfccs)
                categories.append(categories_dict[porcentage])
    #print(data)
    return np.array(data), np.array(categories)

def file_names_to_csv2():
    """categories_dict = {
        '_000': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_010': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_020': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_030': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_040': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_050': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_060': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_070': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_080': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_090': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        '_100': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }"""
    categories_dict = {
        '_000': [1, 0],
        '_010': [1, 0],
        '_020': [1, 0],
        '_030': [0, 1],
        '_040': [0, 1],
        '_050': [0, 1],
        '_060': [0, 1],
        '_070': [0, 1],
        '_080': [0, 1],
        '_090': [0, 1],
        '_100': [0, 1]
    }
    scaler=MinMaxScaler(feature_range=(0,1))
    data = []
    categories = []
    ruta="reverse"
    #ruta="entrenamiento_reducido"
    porcentages = os.listdir("./{}/".format(ruta))
    for porcentage in porcentages:
        audios = os.listdir("./{}/{}".format(ruta,porcentage))
        for audio in audios:
            audio_data, samplerate = sf.read(
                "./{}/{}/{}".format(ruta,porcentage, audio), dtype='float32')
            data_22k = librosa.resample(audio_data.T, samplerate, 22050)
            mfccs = np.mean(librosa.feature.mfcc(
                y=data_22k, sr=samplerate, n_mfcc=40).T, axis=0)
            data.append(mfccs)
            categories.append(categories_dict[porcentage])
    #print(data)
    #return scaler.fit_transform(np.array(data)), np.array(categories)
    return np.array(data), np.array(categories)


#scaler.fit_transform(np.array(data.append(mfccs)))
