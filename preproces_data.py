import csv
import operator

import librosa
import numpy as np
import pandas as pd
import soundfile as sf
from keras.layers import (Activation, Convolution2D, Dense, Dropout, Flatten,
                          MaxPooling2D)
from keras.models import Sequential
from keras.optimizers import Adam
from keras.utils import np_utils, to_categorical
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

data = pd.read_csv("./data.csv", sep=",")

# la frecuencia  de los porcentajes de llenado de gas licuado
print("\nla frecuencia  de los porcentajes de llenado de gas licuado\n")
print(pd.value_counts(data['p']))
print("\n----------------------------\n")

# tabla de frecuencia relativa por %
print("\ntabla de frecuencia relativa por \n")
print(100 * data['p'].value_counts() / len(data['p']))
print("\n----------------------------\n")


def parser(w, t, p, r):
    # function to load files and extract features
    file_name = "w={}t={}f=0{}r={}.wav".format(
        w, t, p, "0{}".format(r) if r < 10 else r)

    # handle exception to check if there isn't a file which is corrupted
    try:
        data, samplerate = sf.read(
            './data/{}'.format(file_name), dtype='float32')
        data = data.T
        # here kaiser_fast is a technique used for faster extraction
        X = librosa.resample(data, samplerate, 22050)
        # we extract mfcc feature from data
        mfccs = np.mean(librosa.feature.mfcc(
            y=X, sr=samplerate, n_mfcc=40).T, axis=0)
    except Exception as e:
        print("Error encountered while parsing file: {} {}".format(file_name, e))
        return None
    return mfccs


# Entrenamiento
# Iteracion sobre cada row para agregar la columna representando el audio
data = data.assign(
    feature=pd.Series(np.vectorize(parser, otypes=[np.ndarray])
    (data['w'],
    data['t'],
    data['p'],
    data['r'])))

data.to_csv('preprocesedData.csv')