index = 0
score = [100, 0]
while True:
    model_aux = Sequential()
    model_aux.add(Dense(16, input_dim=40, activation='relu'))
    model_aux.add(Dense(256, activation="relu"))
    model_aux.add(Dense(1))
    model_aux.add(Dense(11, activation="softmax"))
    
    model_aux.compile(loss='categorical_crossentropy',metrics=['accuracy'],optimizer='rmsprop')
    model_aux.fit(X_train, y_train, batch_size=4, epochs=100,validation_data=(X_test, y_test))
    score_aux = model_aux.evaluate(X_test, y_test, batch_size=2,verbose=1)
    print('--------------evaluate')
    print('score_aux: {}'.format(score_aux))
    print('model_aux: {}'.format(model_aux))
    print('score_aux 0: {}'.format(score_aux[0]))
    print('score 0: {}'.format(score[0]))
    if score_aux[0] < score[0]:
        score = score_aux
        model = model_aux
    if index == 12:
        break 
    index +=1
